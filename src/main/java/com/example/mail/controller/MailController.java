package com.example.mail.controller;

import com.example.mail.entity.Mail;
import com.example.mail.service.MailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MailController {
    @Autowired
    private MailServiceImpl mailService;

    @RequestMapping("/test")
    public String test() {
        for (int i = 0; i < 3; i++) {
            Mail mail = new Mail();
            mail.setTo("1329926763@qq.com");
            mail.setText("HHH");
            mail.setSubject("title");
            mailService.sendMail(mail);
        }
        return "ok";
    }
}
