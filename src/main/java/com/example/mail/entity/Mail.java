package com.example.mail.entity;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

@Data
public class Mail {
    private String id;
    private String from;
    private String to;
    private String subject;
    private String text;
    private Date sendDate;
    private String cc;//抄送，多个邮箱用逗号隔开
    private String bcc;//密送，多个邮箱用逗号隔开
    private String statue;//状态
    private String error;
    private MultipartFile[] multipartFiles;//附件
}