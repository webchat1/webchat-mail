package com.example.mail.config.mq.consumer;

import com.example.mail.entity.UserEntity;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class consumer1 {
    @RabbitListener(bindings = {
            @QueueBinding(value = @Queue(value = "test1", durable = "true", autoDelete = "false"),
                    exchange = @Exchange(value = "exchange3", type = ExchangeTypes.TOPIC),
                    key = {"user.#"}  //路由键
            )
    })
    public void test(UserEntity testMessage, Message message, Channel channel) throws IOException {
        System.out.println(testMessage);
        System.out.println(channel);
        System.out.println(message);


        if (doSomething()) {
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } else {
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
        }


    }

    public boolean doSomething() {
        // 业务操作
        return true;
    }
}
