package com.example.mail.service;

import com.example.mail.entity.Mail;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class MailServiceImplTest {

    @Autowired
    private MailServiceImpl mailService;

    @Test
    void sendMail() {
        for (int i = 0; i < 10; i++) {
            Mail mail = new Mail();
            mail.setTo("1329926763@qq.com");
            mail.setText("HHH");
            mailService.sendMail(mail);
        }

    }
}